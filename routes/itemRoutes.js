const express = require("express");
const router = express.Router();
const ItemController = require("../controllers/itemControllers")
const auth = require("../auth");


router.post("/addItem",auth.verify, (req,res) => {
	const data = {
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin){
		ItemController.addItem(req.body).then(result => res.send(result));
	} else {
		res.send(false)
	}
} )


router.get("/allItems", (req, res) => {
	ItemController.getAllItems().then(result => res.send(result));
});


router.get("/:itemId", (req, res) => {
	ItemController.getItem(req.params.itemId).then(result => res.send(result))
});


router.put("/updateItem/:itemId",auth.verify, (req,res) => {
	const data = {
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin){
		ItemController.updateItem(req.params.itemId,req.body).then(result => res.send(result));
	} else {
		res.send(false)
	}
} )

router.put("/archive/:itemId",auth.verify, (req,res) => {
	const data = {
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin){
		ItemController.archiveItem(req.params.itemId,req.body).then(result => res.send(result));
	} else {
		res.send(false)
	}
} )




module.exports = router;

