const User = require("../models/User");
const Item = require("../models/Item");
const bcrypt = require('bcrypt');
const auth = require("../auth");

module.exports.checkUsernameExists = (reqBody) => {
	return User.find({ username: reqBody.username }).then(result => {
		if(result.length > 0 ){
			return true;
		}else{
			return false;
		}
	})
}

module.exports.registerUser = (reqBody) => {

	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		username: reqBody.username,
		mobileNo: reqBody.mobileNo,
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10)
	})

	return newUser.save().then((user, error) => {	
		if(error) {
			return false;
		} else {
			return true
		}
	})
}

module.exports.loginUser = (reqBody) => {
	return User.findOne({ username: reqBody.username }).then(result => {
		if(result == null) {
			return false;
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
			
			if(isPasswordCorrect){
				return { accessToken : auth.createAccessToken(result.toObject())}
			} else {
				return false;
			}
		};
	});
};

module.exports.updateUserType = (userId, reqBody) => {
	let updatedUserType = {
		isAdmin: reqBody.isAdmin
	};

	return User.findByIdAndUpdate(userId, updatedUserType).then((userId, error) => {
		if(error) {
			return false;
		} else {
			return true;
		}
	})
}

module.exports.createOrder = async (data) => {

			let isUserUpdated = await User.findById(data.userId).then( user => {
				//console.log(user);
				user.orderItems.push({ itemId: data.orderItems.itemId});

				return user.save().then((user, err)=> { return (err) ? false : true })
			});

			//data.orderItems.forEach()

			//console.log(typeof data.orderItems);

			let isItemUpdated = await Item.findById(data.orderItems.itemId).then(item => {
				//console.log(item);
				if(data.orderItems.quantity <= item.quantity ){
					item.customers.push({ userId: data.userId });
					//console.log(data.orderItems.quantity);
					item.quantity -= data.orderItems.quantity

					return item.save().then((item, err)=> { return (err) ? false : true })
				}
			});

			if(isUserUpdated && isItemUpdated){
				return true;
			}else {
				return false;
			}
}

// module.exports.createOrder = (data) => {
// 	//console.log(data);
// 	return isUserUpdated = User.findById(data.userId).then( user => {
			
// 			 data.orderItems.forEach(element => {
// 			 	//console.log(element)
// 			 	user.orderItems.push(element);
// 			 });	
				
// 				let ctr =  data.orderItems.length;
// 				//console.log(ctr);
// 				let isItemUpdated = true;
// 				for (let i = 0; i < ctr; i++) {
// 				//console.log(data);
// 				   //console.log(data.orderItems[i].itemId);
// 				   Item.findById(data.orderItems[i].itemId).then(dbItem => {
// 					   		//console.log(dbItem);
// 						   if(data.orderItems[i].quantity <= dbItem.quantity) {
// 						    	dbItem.quantity -= parseInt(data.orderItems[i].quantity);
// 						    	dbItem.customers.push({userId: data.userId});
// 						    	//console.log("hello");
// 						   }else{
// 						     isItemUpdated = false
// 						   }
// 						   //console.log(isItemUpdated);
// 						   //console.log(i === ctr-1);


// 						   if(isItemUpdated && i === ctr-1){
// 						       data.orderItems.forEach(userElement => {
// 						       		//console.log(userElement);
// 						       		let isItemUpdated = Item.findById(userElement.itemId).then(dbItem => {
// 						       		//console.log(userElement.itemId);
// 						       		//console.log(userElement.quantity);
// 						       		//console.log(dbItem.quantity);
// 						       			if(userElement.quantity <= dbItem.quantity){
// 						       				dbItem.quantity -= parseInt(userElement.quantity);
// 						       				dbItem.customers.push({userId: data.userId});		 
// 						       			}		

// 						       			return dbItem.save().then((dbItem, err)=> { return (err) ? false : true })
// 						       		})
// 						       	})
						       
// 						   	}
// 						   	else if(i === ctr-1) {
// 						   		//console.log("end");
// 						   		return false;
// 					   		}

// 					});
			
// 				}
	
// 		if(isUserUpdated && isItemUpdated === false){
// 			return true;
// 		}else {
// 			return false;
// 		}	
// 		//console.log(data.orderItems);
// 	});
	
// }



module.exports.getOrders = (data) => {
		return User.findById(data.userId).then(result => {
			return result.username + result.orderItems;
	});
}

module.exports.getAllOrders = () => {
		return User.find({orderItems: {$exists : true, $not: {$size: 0}}}, {username: 1, orderItems: 1}).then(result => {
				return result;	
	});
}
